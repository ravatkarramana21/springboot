import { Component } from '@angular/core';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrl: './products.component.css'
})
export class ProductsComponent {

  products = [
    {
      name: 'MRF Bat',
      description: 'Made from good quality Grade 4 natural English Willow.',
      price: 2500,
      imageUrl: './assets/MRF.webp'
    },
    {
      name: 'MRF Gloves',
      description: 'Elasticated texture for slip-on wear and quick fit for stable fitting.',
      price: 500,
      imageUrl: './assets/Gloves.jpg'
    },
    {
      name: 'MRF Pads',
      description: ' Material: PVC and Foam, Pad Type: Batting pads',
      price: 750,
      imageUrl: './assets/Pads.jpg'
    },
    {
      name: 'CEAT Bat',
      description: 'Made from good quality Grade 4 natural English Willow.',
      price: 2000,
      imageUrl: './assets/CEAT.png'
    },
    {
      name: 'CEAT Gloves',
      description: 'Elasticated texture for slip-on wear and quick fit for stable fitting.',
      price: 400,
      imageUrl: './assets/Gloves2.jpg'
    },
    {
      name: 'CEAT Pads',
      description: 'Material: PVC and Foam, Pad Type: Batting pads',
      price: 600,
      imageUrl: './assets/Pads2.jpg'
    }
  ];
}



