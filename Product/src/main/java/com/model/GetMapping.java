package com.model;

public @interface GetMapping {

	String value();

}
